package test.java.com.example.theorytests;

import com.example.theorytests.model.Location;
import com.example.theorytests.LocationService;
import com.example.theorytests.NearService;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class NearServiceTest {
    private LocationService mockLocationService;
    private NearService service;
    @BeforeEach
    public void setUp()
    {
        mockLocationService = Mockito.mock(LocationService.class);
        service = new NearService(mockLocationService);
    }


    @org.junit.jupiter.api.Test
    void isNearToWhenDistanceIsZero() {
        Location location = new Location(0,0);
        when(mockLocationService.getLocation()).thenReturn(location);
        assertEquals(1,service.isNearTo(location));

    }

    @org.junit.jupiter.api.Test
    void isNearToWhenDistanceIsLessThan10Meters() {
        Location location = new Location(0,0); //where we are
        Location anotherLocation = new Location (4,4); //the distance from we want to measure distance
        when(mockLocationService.getLocation()).thenReturn(location);
        assertEquals(1,service.isNearTo(anotherLocation));

    }
    @org.junit.jupiter.api.Test
    void isNearToWhenDistanceIsLessThan20MetersButMoreThan10() {
        Location location = new Location(0,0); //where we are
        Location anotherLocation = new Location (100,100); //the distance from we want to measure distance
        when(mockLocationService.getLocation()).thenReturn(location);
        assertEquals(2,service.isNearTo(anotherLocation));
    }
}
package com.example.theorytests.model;

public class Location {
    private int x;
    private int y;

    public Location(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public double getDistanceFromLocation(Location location){
        return Math.sqrt(Math.pow(location.x-x,2))+Math.pow((location.y-y),2);
    }


}

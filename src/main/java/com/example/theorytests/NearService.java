package com.example.theorytests;

import com.example.theorytests.model.Location;

public class NearService {
  private final LocationService locationService;

    public NearService(LocationService locationService) {
        this.locationService = locationService;
    }

    public int isNearTo(Location location){
        Location currentLocation = locationService.getLocation();
        double distance = currentLocation.getDistanceFromLocation(location);

        if (distance < 10) {
            return 1;
        } else if (distance < 20){
            return 2;
        } else {
            return 3;
        }
    }
}

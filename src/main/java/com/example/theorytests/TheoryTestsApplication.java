package com.example.theorytests;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TheoryTestsApplication {

    public static void main(String[] args) {
        SpringApplication.run(TheoryTestsApplication.class, args);
    }

}
